<?php

//==============
//  DEFINE PATH
//==============

define('BASE_PATH',     realpath(dirname(__FILE__)));
define("LIB",           BASE_PATH."/lib/");
define("VIEWS",         BASE_PATH."/views/");

define("CONFIG",        BASE_PATH."/config.ini");

//==============
//   AUTOLOAD
//==============

require LIB.'autoload.php';

//==============
//    ROUTER
//==============

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['user'])){
    $nav_btn = '<a href="/pridat-ponuku" class="button">Pridať pracovnú ponuku</a>';
} else {
    $nav_btn = '<a href="/registracia-spolocnosti" class="button">Registrácia firmy</a>';
}

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', 'HANDLERS\home/index');
    $r->addRoute('POST', '/', 'HANDLERS\home/notification');

    $r->addRoute('GET', '/ponuky-prace', 'HANDLERS\offers/index');
    $r->addRoute('GET', '/ponuky-prace/get/{offset:\d+}[/{limit:\d+}]', 'HANDLERS\offers/get_offers');
    $r->addRoute('GET', '/ponuky-prace/hladat', 'HANDLERS\offers/search');
    $r->addRoute('GET', '/ponuka-prace/{id:\d+}', 'HANDLERS\offers/index_getOffer');

    $r->addRoute('GET', '/pridat-ponuku', 'HANDLERS\offers/index_addOffer');
    $r->addRoute('POST', '/pridat-ponuku', 'HANDLERS\offers/receiver_addOffer');

    $r->addRoute('GET', '/spolocnosti', 'HANDLERS\companies/index');
    $r->addRoute('GET', '/spolocnost/{id:\d+}', 'HANDLERS\companies/getCompany');
    
    $r->addRoute('GET', '/o-nas', 'HANDLERS\about/index');
    
    $r->addRoute('GET', '/registracia-spolocnosti', 'HANDLERS\registration/index_signIn');
    $r->addRoute('GET', '/registracia-spolocnosti/email/{email}', 'HANDLERS\registration/check_email');
    $r->addRoute('GET', '/registracia-spolocnosti/ico/{ico}', 'HANDLERS\registration/check_ico');
    $r->addRoute('POST', '/registracia-spolocnosti', 'HANDLERS\registration/receiver_signIn');

    $r->addRoute('GET', '/prihlasenie', 'HANDLERS\registration/index_signUp');
    $r->addRoute('POST', '/prihlasenie', 'HANDLERS\registration/receiver_signUp');
    $r->addRoute('GET', '/odhlasenie', 'HANDLERS\registration/index_signOut');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        $notification = array(
            'header' => 'Chyba 404',
            'message' => 'Stránka ktorú ste chceli zobraziť neexistuje, boli ste presmerovaný na úvodnú stránku',
            'timer' => 5
        );

        MICHAL53Q\helper::redirect('/', $notification);
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);
        break;
}