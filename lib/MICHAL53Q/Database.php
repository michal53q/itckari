<?php

namespace MICHAL53Q;

class Database 
{
    private $conn;

    public function __construct()
    {
        if(!isset($this->conn)) {
            $this->connect();
        }
    }

    private function connect()
    {
        $config = parse_ini_file(CONFIG, true);

        $host = $config['database']['host'];
        $db = $config['database']['db'];
        $user = $config['database']['user'];
        $pass = $config['database']['pass'];

        $this->conn = new \PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function add_user($input)
    {
        $stmt = $this->conn->prepare("INSERT INTO 
        USERS (`reg_date`, `email`, `hash`, `name`, `ico`, `ic_dph`, `adresa`, `mesto`, `psc`, `telefon`, `web`, `charakteristika`, `oblast`, `zamestnanci`, `logo`, `suhlas`) 
        VALUES (:reg_date, :email, :hash, :name, :ico, :ic_dph, :adresa, :mesto, :psc, :telefon, :web, :charakteristika, :oblast, :zamestnanci, :logo, :suhlas)");
        
        $stmt->bindParam(':reg_date', $input['reg_date']);
        $stmt->bindParam(':email', $input['email']);
        $stmt->bindParam(':hash', $input['password']);
        $stmt->bindParam(':name', $input['name']);
        $stmt->bindParam(':ico', $input['ico']);
        $stmt->bindParam(':ic_dph', $input['icDph']);
        $stmt->bindParam(':adresa', $input['adresa']);
        $stmt->bindParam(':mesto', $input['mesto']);
        $stmt->bindParam(':psc', $input['psc']);
        $stmt->bindParam(':telefon', $input['tel']);
        $stmt->bindParam(':web', $input['web']);
        $stmt->bindParam(':charakteristika', $input['popis']);
        $stmt->bindParam(':oblast', $input['oblast']);
        $stmt->bindParam(':zamestnanci', $input['zamestnanci']);
        $stmt->bindParam(':logo', $input['logo']);
        $stmt->bindParam(':suhlas', $input['suhlas']);

        $stmt->execute();
    }

    public function login($input)
    {
        $stmt = $this->conn->prepare("SELECT id, hash FROM USERS WHERE email = :email");
        $stmt->bindValue(':email', $input['email'], \PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function check_email($input)
    {
        $stmt = $this->conn->prepare("SELECT email FROM USERS WHERE email = :email");
        $stmt->bindValue(':email', $input, \PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if(empty($result)) {
            return 'false';
        }
        else {
            return 'true';
        }
    }

    public function check_ico($input)
    {
        $stmt = $this->conn->prepare("SELECT ico FROM USERS WHERE ico = :ico");
        $stmt->bindValue(':ico', $input, \PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if(empty($result)) {
            return 'false';
        }
        else {
            return 'true';
        }
    }

    public function get_company($id)
    {
        $stmt = $this->conn->prepare("SELECT name, logo, adresa, mesto, zamestnanci, web, charakteristika FROM USERS WHERE id = :id");
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function add_offer($input)
    {
        $stmt = $this->conn->prepare("INSERT INTO 
        OFFERS (`user_id`, `created_on`, `description_short`, `category`, `name`, `location`, `salary`, `time`, `scope`, `requirements`, `benefits`) 
        VALUES (:user_id, :created_on, :description_short, :category, :name, :location, :salary, :time, :scope, :requirements, :benefits)");
        
        $stmt->bindParam(':user_id', $input['user_id']);
        $stmt->bindParam(':created_on', $input['offer_created']);
        $stmt->bindParam(':description_short', $input['offer_description_short']);
        $stmt->bindParam(':category', $input['offer_category']);
        $stmt->bindParam(':name', $input['offer_name']);
        $stmt->bindParam(':location', $input['offer_location']);
        $stmt->bindParam(':salary', $input['offer_salary']);
        $stmt->bindParam(':time', $input['offer_time']);
        $stmt->bindParam(':scope', $input['offer_scope']);
        $stmt->bindParam(':requirements', $input['offer_requirements']);
        $stmt->bindParam(':benefits', $input['offer_benefits']);

        $stmt->execute();
    }

    public function get_offer_full($id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM view_offers WHERE id = :id");
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($result as $id => $row){
            switch($row['location']) {
                case 'bratislavsky-kraj':
                    $result[$id]['location'] = 'Bratislavský kraj';
                    break;
                case 'trnavsky-kraj':
                    $result[$id]['location'] = 'Trnavský kraj';
                    break;
                case 'trenciansky-kraj':
                    $result[$id]['location'] = 'Trenčiansky kraj';
                    break;
                case 'nitriansky-kraj':
                    $result[$id]['location'] = 'Nitriansky kraj';
                    break;
                case 'zilinsky-kraj':
                    $result[$id]['location'] = 'Žilinský kraj';
                    break;
                case 'banskobystricky-kraj':
                    $result[$id]['location'] = 'Banskobystrický kraj';
                    break;
                case 'kosicky-kraj':
                    $result[$id]['location'] = 'Košický kraj';
                    break;
                case 'presovsky-kraj':
                    $result[$id]['location'] = 'Prešovský kraj';
                    break;
            }
        }

        return $result;
    }

    public function get_company_categoryCount()
    {
        $stmt = $this->conn->prepare("SELECT `oblast`, count(*) as 'count' FROM users GROUP BY `oblast`");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $return = array();

        foreach($result as $row){
            $return[$row['oblast']] = $row['count'];
        }

        return $return;
    }

    public function get_offer_categoryCount()
    {
        $stmt = $this->conn->prepare("SELECT `category`, count(*) as 'count' FROM offers GROUP BY `category`");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $return = array();

        foreach($result as $row){
            $return[$row['category']] = $row['count'];
        }

        return $return;
    }

    public function get_offer_count()
    {
        $stmt = $this->conn->prepare("SELECT COUNT(*) as count FROM view_offers");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function get_offer_short($length, $offset = null)
    {
        if($offset != null)
            $addOffset = "OFFSET ".$offset;
        else 
            $addOffset = "";

        $stmt = $this->conn->prepare("SELECT `id`, `logo`, `name`, `location`, `salary`, `time`, `created_on` FROM `view_offers`
        ORDER BY `created_on` DESC LIMIT $length $addOffset");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($result as $id => $row){
            switch($row['location']) {
                case 'bratislavsky-kraj':
                    $result[$id]['location'] = 'Bratislavský kraj';
                    break;
                case 'trnavsky-kraj':
                    $result[$id]['location'] = 'Trnavský kraj';
                    break;
                case 'trenciansky-kraj':
                    $result[$id]['location'] = 'Trenčiansky kraj';
                    break;
                case 'nitriansky-kraj':
                    $result[$id]['location'] = 'Nitriansky kraj';
                    break;
                case 'zilinsky-kraj':
                    $result[$id]['location'] = 'Žilinský kraj';
                    break;
                case 'banskobystricky-kraj':
                    $result[$id]['location'] = 'Banskobystrický kraj';
                    break;
                case 'kosicky-kraj':
                    $result[$id]['location'] = 'Košický kraj';
                    break;
                case 'presovsky-kraj':
                    $result[$id]['location'] = 'Prešovský kraj';
                    break;
            }
        }

        return $result;
    }

    public function search_offer_short($conditions, $length)
    {
        $addConditions = "";
        $addAnd = false;

        if(($conditions['location'] != null && $conditions['category'] != null) || 
           ($conditions['location'] != null && $conditions['search'] != null) || 
           ($conditions['search'] != null && $conditions['category'] != null)) {
                $addAnd = true;
            }

        if($conditions['location'] != null)
            $addConditions .= "`location` = '".$conditions['location']."'". (($addAnd) ? " AND " : "");

        if($conditions['category'] != null)
            $addConditions .= "`category` = '".$conditions['category']."'". (($addAnd && $conditions['search'] != null) ? " AND " : "");

        if($conditions['search'] != null)
            $addConditions .= "`name` LIKE '%".$conditions['search']."%'";

        $stmt = $this->conn->prepare("SELECT COUNT(*) as count, `id`, `logo`, `name`, `location`, `salary`, `time`, `created_on` FROM `view_offers`
        WHERE ".$addConditions." ORDER BY `created_on` DESC LIMIT $length");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function get_company_count()
    {
        $stmt = $this->conn->prepare("SELECT COUNT(*) as count FROM users");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function get_company_short($length)
    {
        $stmt = $this->conn->prepare("SELECT `id`, `logo`, `name`, `charakteristika` FROM users ORDER BY `name` ASC LIMIT $length");
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }
}