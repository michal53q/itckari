<?php

namespace MICHAL53Q;

class helper {
    public static function redirect($location, $notification = array())
    {
        if(!empty($notification))
            $_SESSION['notification'] = $notification;

        header('Location: '.$location);
        die();
    }
}