# itckari

# Database set-up
## view_offers
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_offers` AS select `offers`.`id` AS `id`,`offers`.`user_id` AS `user_id`,`offers`.`created_on` AS `created_on`,`offers`.`description_short` AS `description_short`,`offers`.`category` AS `category`,`offers`.`name` AS `name`,`offers`.`location` AS `location`,`offers`.`salary` AS `salary`,`offers`.`time` AS `time`,`offers`.`scope` AS `scope`,`offers`.`requirements` AS `requirements`,`offers`.`benefits` AS `benefits`,`users`.`name` AS `comp_name`,`users`.`adresa` AS `adresa`,`users`.`mesto` AS `mesto`,`users`.`psc` AS `psc`,`users`.`web` AS `web`,`users`.`charakteristika` AS `charakteristika`,`users`.`oblast` AS `oblast`,`users`.`zamestnanci` AS `zamestnanci`,`users`.`logo` AS `logo` from (`offers` join `users` on((`offers`.`user_id` = `users`.`id`)))

## offers
CREATE TABLE `offers` (
 `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
 `user_id` int(6) NOT NULL,
 `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 `description_short` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `category` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `name` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `location` varchar(255) COLLATE utf8_slovak_ci DEFAULT NULL,
 `salary` int(10) NOT NULL,
 `time` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `scope` mediumtext COLLATE utf8_slovak_ci NOT NULL,
 `requirements` mediumtext COLLATE utf8_slovak_ci NOT NULL,
 `benefits` mediumtext COLLATE utf8_slovak_ci NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci

## users
CREATE TABLE `users` (
 `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
 `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 `email` varchar(50) COLLATE utf8_slovak_ci DEFAULT NULL,
 `hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
 `name` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `ico` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `ic_dph` varchar(255) COLLATE utf8_slovak_ci DEFAULT NULL,
 `adresa` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `mesto` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `psc` int(10) NOT NULL,
 `telefon` varchar(255) COLLATE utf8_slovak_ci DEFAULT NULL,
 `web` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `charakteristika` mediumtext COLLATE utf8_slovak_ci NOT NULL,
 `oblast` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `zamestnanci` int(10) NOT NULL,
 `logo` varchar(255) COLLATE utf8_slovak_ci NOT NULL,
 `suhlas` tinyint(1) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci
