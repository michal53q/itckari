<?php

namespace HANDLERS;

class companies 
{
    protected $db;

    public function __construct()
    {
        if(!isset($this->db)){
            $this->db = new \MICHAL53Q\Database();
        }
    }

    public function index() 
    {
        $getCompanies = $this->db->get_company_short(10);
        $getCompaniesCount = $this->db->get_company_count();
        $getCategoryCounts = $this->db->get_company_categoryCount();

        $count_informatika = (isset($getCategoryCounts['informatika'])) ? $getCategoryCounts['informatika'] : 0;
        $count_design = (isset($getCategoryCounts['graficky-design'])) ? $getCategoryCounts['graficky-design'] : 0;
        $count_elektronika = (isset($getCategoryCounts['elektronika'])) ? $getCategoryCounts['elektronika'] : 0;
        $count_elektrotechnika = (isset($getCategoryCounts['elektrotechnika'])) ? $getCategoryCounts['elektrotechnika'] : 0;
        $count_automatizacia = (isset($getCategoryCounts['automatizacia'])) ? $getCategoryCounts['automatizacia'] : 0;
        $count_telekomunikacia = (isset($getCategoryCounts['telekomunikacia'])) ? $getCategoryCounts['telekomunikacia'] : 0;

        $add_JS = '<script src="../includes/companies.js"></script>';

        include VIEWS.'includes/header.phtml';

        include VIEWS.'companies.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function getCompany($id)
    {
        $getCompany = $this->db->get_company($id);

        if(empty($getCompany)){
            $notification = array(
                'header' => 'Chyba 404',
                'message' => 'Stránka ktorú ste chceli zobraziť neexistuje, boli ste presmerovaný na úvodnú stránku',
                'timer' => 5
            );
    
            \MICHAL53Q\helper::redirect('/', $notification);
        } else {
            $getCompany = $getCompany[0];
        }

        switch($getCompany['zamestnanci']){
            case 0:
                $company_employees = '0 zamestnancov';
                break;
            case 1:
                $company_employees = '1 zamestnanec';
                break;
            case 2:
                $company_employees = '2 zamestnanci';
                break;
            case 3:
                $company_employees = '3-4 zamestnanci';
                break;
            case 5:
                $company_employees = '5-9 zamestnancov';
                break;
            case 10:
                $company_employees = '10-19 zamestnancov';
                break;
            case 20:
                $company_employees = '20-24 zamestnancov';
                break;
            case 25:
                $company_employees = '25-49 zamestnancov';
                break;
            case 50:
                $company_employees = '50-99 zamestnancov';
                break;
            case 100:
                $company_employees = '100-149 zamestnancov';
                break;
            case 150:
                $company_employees = '150-199 zamestnancov';
                break;
            case 200:
                $company_employees = '200-249 zamestnancov';
                break;
            case 250:
                $company_employees = '250-499 zamestnancov';
                break;
            case 500:
                $company_employees = '500-999 zamestnancov';
                break;
            case 1000:
                $company_employees = '1000 a viac zamestnancov';
                break;
        }

        $company_name = $getCompany['name'];
        $company_logo = $getCompany['logo'];
        $company_address = $getCompany['adresa'].', '.$getCompany['mesto'];
        $company_url = $getCompany['web'];
        $company_description = $getCompany['charakteristika'];

        include VIEWS.'includes/header.phtml';

        include VIEWS.'templates/company.phtml';

        include VIEWS.'includes/footer.phtml';
    }
}