<?php

namespace HANDLERS;

class registration 
{
    protected $db;

    public function __construct()
    {
        if(!isset($this->db)){
            $this->db = new \MICHAL53Q\Database();
        }
    }

    public function index_signUp() 
    {
        $add_JS = '<script src="../includes/registration.js"></script>';

        include VIEWS.'includes/header.phtml';

        include VIEWS.'signUp.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function index_signIn() 
    {
        $add_JS = '<script src="../includes/registration.js"></script>';
        include VIEWS.'includes/header.phtml';

        include VIEWS.'signIn.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function index_signOut() 
    {
        if(isset($_SESSION['user']))
            unset($_SESSION['user']);        

        $notification = array(
            'header' => 'Presmerovanie',
            'message' => 'Boli ste úspešne odhlásený',
            'timer' => 5
        );

        \MICHAL53Q\helper::redirect('/', $notification);
    }

    public function receiver_signUp() 
    {
        $getData = $this->db->login($_POST);

        if(!empty($getData) && password_verify($_POST['password'], $getData[0]['hash'])) {
            $_SESSION['user'] = array(
                'id' => $getData[0]['id'],
                'email' => $_POST['email']
            );
            
            $result = array(
                "code" => 200,
                "description" => "login successful",
                "email" => $_POST['email']
            );
        } else {
            $result = array(
                "code" => 400,
                "description" => "Wrong email or password!"
            );
        }

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function receiver_signIn() 
    {
        $result_email = $this->db->check_email($input);
        $result_ico = $this->db->check_ico($input);

        if($result_email == "true" || $result_ico == "true")
            die();

        if(isset($_FILES['logo'])){
            $name = $_FILES['logo']['name']; 
            $target = BASE_PATH."/includes/uploads/".$name;
            move_uploaded_file( $_FILES['logo']['tmp_name'], $target);
        } else {
            $fileName = strripos($_POST['logo'],"/");
            $filePath = BASE_PATH.substr($_POST['logo'], 0, $fileName) . "/companies/". $_POST['ico'] . substr($_POST['logo'], $fileName);
            if(!file_exists(BASE_PATH.'/includes/uploads/companies/'.$_POST['ico']))
                mkdir(BASE_PATH.'/includes/uploads/companies/'.$_POST['ico'], 0777, true);
            rename(BASE_PATH.$_POST['logo'], $filePath);

            $input = array(
                'reg_date' => date("Y-m-d H:i:s"),
                'email' => $_POST['email'],
                'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                'name' => $_POST['name'],
                'ico' => $_POST['ico'],
                'icDph' => $_POST['icDph'],
                'adresa' => $_POST['adresa'],
                'mesto' => $_POST['mesto'],
                'psc' => $_POST['psc'],
                'tel' => $_POST['tel'],
                'web' => $_POST['web'],
                'popis' => $_POST['popis'],
                'oblast' => $_POST['oblast'],
                'zamestnanci' => $_POST['zamestnanci'],
                'logo' => '/includes/uploads/companies/'.$_POST['ico'].'/'.substr($_POST['logo'], $fileName),
                'suhlas' => $_POST['suhlas']
            );

            $this->db->add_user($input);
            die();
        }
    }

    public function check_email($input)
    {
        echo $this->db->check_email($input); 
    }

    public function check_ico($input)
    {
        echo $this->db->check_ico($input); 
    }
}