<?php

namespace HANDLERS;

class about 
{
    public function index()
    {
        include VIEWS.'includes/header.phtml';

        include VIEWS.'about.phtml';

        include VIEWS.'includes/footer.phtml';
    }
}