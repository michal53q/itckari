<?php

namespace HANDLERS;

class offers 
{
    protected $db;

    public function __construct()
    {
        if(!isset($this->db)){
            $this->db = new \MICHAL53Q\Database();
        }
    }

    public function index() 
    {
        $getOffers = $this->db->get_offer_short(10);
        $getOffersCount = $this->db->get_offer_count();

        $add_JS = '<script src="../includes/offers.js"></script>';

        include VIEWS.'includes/header.phtml';

        include VIEWS.'offers.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function index_getOffer($id) 
    {
        $getOffer = $this->db->get_offer_full($id);

        if(empty($getOffer)) {
            $notification = array(
                'header' => 'Chyba 404',
                'message' => 'Stránka ktorú ste chceli zobraziť neexistuje, boli ste presmerovaný na úvodnú stránku',
                'timer' => 5
            );
    
            \MICHAL53Q\helper::redirect('/', $notification);
        }

        $getOffer = $getOffer[0];

        $add_JS = '<script src="../includes/offers.js"></script>';

        switch($getOffer['zamestnanci']){
            case 0:
                $company_employees = '0 zamestnancov';
                break;
            case 1:
                $company_employees = '1 zamestnanec';
                break;
            case 2:
                $company_employees = '2 zamestnanci';
                break;
            case 3:
                $company_employees = '3-4 zamestnanci';
                break;
            case 5:
                $company_employees = '5-9 zamestnancov';
                break;
            case 10:
                $company_employees = '10-19 zamestnancov';
                break;
            case 20:
                $company_employees = '20-24 zamestnancov';
                break;
            case 25:
                $company_employees = '25-49 zamestnancov';
                break;
            case 50:
                $company_employees = '50-99 zamestnancov';
                break;
            case 100:
                $company_employees = '100-149 zamestnancov';
                break;
            case 150:
                $company_employees = '150-199 zamestnancov';
                break;
            case 200:
                $company_employees = '200-249 zamestnancov';
                break;
            case 250:
                $company_employees = '250-499 zamestnancov';
                break;
            case 500:
                $company_employees = '500-999 zamestnancov';
                break;
            case 1000:
                $company_employees = '1000 a viac zamestnancov';
                break;
        }

        $company_name = $getOffer['comp_name'];
        $company_address = $getOffer['adresa'] . ", " . $getOffer['mesto'] . ", " . $getOffer['psc'];
        $company_url = $getOffer['web'];
        $company_logo = $getOffer['logo'];
        $company_description = $getOffer['charakteristika'];
        $company_branch = ucfirst($getOffer['oblast']);

        $offer_name = $getOffer['name'];
        $offer_location = !empty($getOffer['location']) ? $getOffer['location'] : $getOffer['mesto'];
        $offer_salary = $getOffer['salary'];
        $offer_time = $getOffer['time'];
        $offer_description_short = $getOffer['description_short'];
        $offer_category = ucfirst($getOffer['category']);
        $offer_created = date("d.m.Y", strtotime($getOffer['created_on']));
        $offer_scope = $getOffer['scope'];
        $offer_requirements = $getOffer['requirements'];
        $offer_benefits = $getOffer['benefits'];

        include VIEWS.'includes/header.phtml';

        include VIEWS.'templates/offer.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function index_addOffer() 
    {
        if(!isset($_SESSION['user'])) {
            $notification = array(
                'header' => 'Chyba 404',
                'message' => 'Stránka ktorú ste chceli zobraziť neexistuje, boli ste presmerovaný na úvodnú stránku',
                'timer' => 5
            );
    
            \MICHAL53Q\helper::redirect('/', $notification);
        }

        $add_JS = '<script src="../includes/offers.js"></script>';

        $company_name = 'Kistler Bratislava s.r.o.';
        $company_address = 'Ševčenkova 34, Bratislava, 85101';
        $company_employees = '100-149 zamestnancov';
        $company_url = 'https://www.kistlerteam.sk/';
        $company_logo = '/includes/uploads/companies/000000/Kistler_Logo_online_RGB.jpg';
        $company_description = "Kistler is global leader in dynamic pressure, force, torque and acceleration measurement. Cutting-edge technologies provide the basis for Kistler's modular systems and services. Kistler Bratislava, dedicated on SW development, is formed by a great team of SW professionals. At present, we work on systems that are used for the next generation engine development by the big automotive companies, real time traffic monitoring and quality control in industrial applications. We are moving our focus to distributed data acquisition systems which can be configured by our customers and interconnected with their systems. Our projects keep up with the pace of innovation and so pay special attention to the new era of Industry 4.0 and cloud computing. We are continuously growing and therefore we are looking for software developers with knowledge of C#/.NET, C++, JavaScript, HTML5, Java, but we provide the opportunity also for junior or senior Software testers. The right person for our company is able not just to learn and evaluate cutting edge technologies, but also to finalize and deliver projects based on them. If you are a team player, who doesn’t just produce code, but also takes care about quality and automation of repetitive tasks, check our webpage and send us your CV today!";
        $company_branch = 'Informatika';
        $offer_created = date("d.m.Y");

        $GLOBALS['nav_btn'] = '<input class="button" type="submit" form="form_addOffer" value="Uložiť ponuku">';

        include VIEWS.'includes/header.phtml';

        include VIEWS.'addOffer.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function receiver_addOffer()
    {
        if(!isset($_SESSION['user'])) {
            $notification = array(
                'header' => 'Chyba 404',
                'message' => 'Stránka ktorú ste chceli zobraziť neexistuje, boli ste presmerovaný na úvodnú stránku',
                'timer' => 5
            );
    
            \MICHAL53Q\helper::redirect('/', $notification);
        }

        $input = array(
            'user_id' => $_SESSION['user']['id'],
            'offer_description_short' => $_POST['offer_description_short'],
            'offer_category' => $_POST['offer_category'],
            'offer_name' => $_POST['offer_name'],
            'offer_location' => $_POST['offer_location'],
            'offer_salary' => $_POST['offer_salary'],
            'offer_time' => $_POST['offer_time'],
            'offer_scope' => $_POST['offer_scope'],
            'offer_requirements' => $_POST['offer_requirements'],
            'offer_benefits' => $_POST['offer_benefits'],
            'offer_created' => date("Y-m-d H:i:s")
        );

        $this->db->add_offer($input);

        $result = array(
            "code" => 200,
            "description" => "Ponuka bola úspešne vytvorená"
        );

        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function search()
    {
        if(!isset($_GET['l']) && !isset($_GET['c']) && !isset($_GET['s'])){
            \MICHAL53Q\helper::redirect('/ponuky-prace');               
        }

    
        $conditions = array(
            'location' => (isset($_GET['l'])) ? $_GET['l'] : null,
            'category' => (isset($_GET['c'])) ? $_GET['c'] : null,
            'search' => (isset($_GET['s'])) ? urldecode($_GET['s']) : null
        );

        $getOffers = $this->db->search_offer_short($conditions , 10);
        $getOffersCount = $getOffers;

        include VIEWS.'includes/header.phtml';

        include VIEWS.'offers.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function get_offers($offset, $limit = 10)
    {
        $getOffers = $this->db->get_offer_short($limit, $offset);

        header('Content-Type: application/json');
        echo json_encode($getOffers);
    }
}