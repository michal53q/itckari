<?php

namespace HANDLERS;

class home 
{
    public $notification;

    protected $db;

    public function __construct()
    {
        if(!isset($this->db)){
            $this->db = new \MICHAL53Q\Database();
        }
    }

    public function index() 
    {
        $getCategoryCounts = $this->db->get_offer_categoryCount();
        $getOffers = $this->db->get_offer_short(4);

        $count_informatika = (isset($getCategoryCounts['informatika'])) ? $getCategoryCounts['informatika'] : 0;
        $count_design = (isset($getCategoryCounts['graficky-design'])) ? $getCategoryCounts['graficky-design'] : 0;
        $count_elektronika = (isset($getCategoryCounts['elektronika'])) ? $getCategoryCounts['elektronika'] : 0;
        $count_elektrotechnika = (isset($getCategoryCounts['elektrotechnika'])) ? $getCategoryCounts['elektrotechnika'] : 0;
        $count_automatizacia = (isset($getCategoryCounts['automatizacia'])) ? $getCategoryCounts['automatizacia'] : 0;
        $count_telekomunikacia = (isset($getCategoryCounts['telekomunikacia'])) ? $getCategoryCounts['telekomunikacia'] : 0;

        $add_JS = '<script src="../includes/home.js"></script>';
        $background = 'home';
        
        if(isset($_SESSION['notification'])){
            $this->notification = 'showNotification("'.$_SESSION['notification']['header'].'", "'.$_SESSION['notification']['message'].'", '.$_SESSION['notification']['timer'].');';
            unset($_SESSION['notification']);
        }

        $executeJS = $this->notification;
        unset($this->notification);

        include VIEWS.'includes/header.phtml';

        include VIEWS.'home.phtml';

        include VIEWS.'includes/footer.phtml';
    }

    public function notification($input = array()) 
    {
        if(empty($input)) {
            $header = $_POST['header'];
            $message = str_replace(PHP_EOL, '\n', $_POST['message']);
            $timer = 5;
        }
        else {
            $header = $input['header'];
            $message = $input['message'];
            $timer = $input['timer'];
        }

        $this->notification = 'showNotification("'.$header.'", "'.$message.'", '.$timer.');';
        $this->index();
    }
}