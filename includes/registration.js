var checkPassword = false;
var checkEmail = false;
var checkIco = false;
var checkTel = true;

window.onload = function() {
    var form_signUp = document.getElementById("form_signUp");
    var form_signIn = document.getElementById("form_signIn");

    if(form_signUp != undefined){
        form_signUp.addEventListener("submit", function (event) {
            event.preventDefault();
    
            sendData('signUp');
        });
    }

    if(form_signIn != undefined){
        form_signIn.addEventListener("submit", function (event) {
            event.preventDefault();
            closeAllNotifications();
            
            if(checkPassword && checkEmail && checkIco && checkTel) {
                sendData('signIn');
            } else {
                var msg_pass, msg_email, msg_ico, msg_tel;
                if(!checkPassword) msg_pass = '\n   - Heslá sa nezhodujú'; else msg_pass = '';
                if(!checkEmail) msg_email = '\n   - Email už je registrovaný'; else msg_email = '';
                if(!checkIco) msg_ico = '\n   - IČO už je registrované'; else msg_ico = '';
                if(!checkTel) msg_tel = '\n   - Telefón obsahuje nepovolené znaky'; else msg_tel = '';

                showNotification('Registrácia nebola úspešná', 'Je nutné upraviť polia:'+msg_pass+msg_email+msg_ico+msg_tel);
            }
        });
    }

    function sendData(formType) {
        var formElement = document.querySelector("form");
        var request = new XMLHttpRequest();

        switch(formType){
            case 'signUp':
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var result = JSON.parse(this.responseText);
                        switch(result['code']) {
                            case 200:
                                redirect('/', 'Prihlásenie bolo úspešné\nSte prihlásený s e-mailom: ' + result['email']);
                                break;
                            case 400:
                                var inputElements = document.getElementsByTagName('INPUT');
                                for(var i = 0; i < inputElements.length; i++) {
                                    if(inputElements[i].type.toLowerCase() == 'email' || inputElements[i].type.toLowerCase() == 'password'){
                                        inputElements[i].style.border = '2px solid red';
                                    }
                                }
                                showNotification('Prihlásenie zlyhalo', 'Zlý e-mail alebo heslo');
                                break;
                        }
                    }
                };
        
                request.open("POST", "/prihlasenie");
                request.send(new FormData(formElement));
                break;
            case 'signIn':
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        redirect('/', 'Registrácia prebehla úspešne\nMôžete sa prihlásiť');
                    }
                };
        
                request.open("POST", "/registracia-spolocnosti");
                request.send(new FormData(formElement));
                break;
        }
    }

    check_fields();
};

function openDialog(x) {
    switch(x) {
        case 'uploadLogo':
            document.getElementById('input_uploadLogo').click();
            break;
    }
}

function handleFiles(x, file) {
    switch(x) {
        case 'uploadLogo':
            var formData = new FormData(); // Currently empty
            var request = new XMLHttpRequest();
            var link = document.getElementById('link_uploadLogo');
            var input = document.getElementById('input_link_uploadLogo');

            request.open('POST', 'http://localhost/registracia-spolocnosti', true);
            formData.append('logo', file[0]);
            request.send(formData);

            link.innerHTML = file[0].name;
            link.href = '/includes/uploads/'+file[0].name;
            input.value = '/includes/uploads/'+file[0].name;

            break;
    }
}

function check_fields() {
    var collection_input = document.getElementsByTagName('input');
    var field_pass = [];
    var field_email;
    var field_ico;
    var field_tel;

    if(collection_input.length > 0){
        for(var i = 0; i < collection_input.length; i++) {
            if(collection_input[i].type.toLowerCase() == 'password') {
                field_pass.push(collection_input[i]);
            } else if(collection_input[i].type.toLowerCase() == 'email') {
                field_email = collection_input[i];
            } else if(collection_input[i].name.toLowerCase() == "ico") {
                field_ico = collection_input[i];
            } else if(collection_input[i].name.toLowerCase() == "tel") {
                field_tel = collection_input[i];
            }
        }

        field_pass[0].addEventListener("change", function(){
            if(field_pass[0].value != "" && field_pass[1].value != ""){
                if(field_pass[0].value != field_pass[1].value) {
                    field_pass[0].style.border = '2px solid red';
                    field_pass[1].style.border = '2px solid red';
                    checkPassword = false;
                } else {
                    field_pass[0].style.border = '';
                    field_pass[1].style.border = '';
                    checkPassword = true;
                }
            }
        });

        field_pass[1].addEventListener("change", function(){
            if(field_pass[0].value != "" && field_pass[1].value != ""){
                if(field_pass[0].value != field_pass[1].value) {
                    field_pass[0].style.border = '2px solid red';
                    field_pass[1].style.border = '2px solid red';
                    checkPassword = false;
                } else {
                    field_pass[0].style.border = '';
                    field_pass[1].style.border = '';
                    checkPassword = true;
                }
            }
        });

        field_email.addEventListener("change",  function(){
            if(field_email.value != "") {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if(this.responseText == "false") {
                            field_email.style.border = '';
                            checkEmail = true;
                        } else {
                            field_email.style.border = '2px solid red';
                            checkEmail = false;
                        }
                    }
                };
                request.open("GET", "/registracia-spolocnosti/email/" + field_email.value);
                request.send();
            }
        });

        field_ico.addEventListener("change",  function(){
            if(field_ico.value != "") {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if(this.responseText == "false") {
                            field_ico.style.border = '';
                            checkIco = true;
                        } else {
                            field_ico.style.border = '2px solid red';
                            checkIco = false;
                        }
                    }
                };
                request.open("GET", "/registracia-spolocnosti/ico/" + field_ico.value);
                request.send();
            }
        });

        field_tel.addEventListener("change", function(){
            if(field_tel.value != ""){
                var reg = /^[0-9\+\/\ ]+$/;
                if(reg.test(field_tel.value)){
                    field_tel.style.border = '';
                    checkTel = true;
                } else {
                    field_tel.style.border = '2px solid red';
                    checkTel = false;
                }
            } else {
                field_tel.style.border = '';
                checkTel = true;
            }
        });
    }
}