var checkName = false;
var checkSalary = false;
var checkDescriptionShort = false;
var checkScope = false;
var checkRequirements = false;
var checkBenefits = false;
var dropdownSelected = {};
var activeElement;
var switchData = {};

window.onload = function() {
    var form_addOffer = document.getElementById("form_addOffer");
    var e_switch = document.getElementsByTagName("switch");

    if(form_addOffer != undefined){
        form_addOffer.addEventListener("submit", function (event) {
            event.preventDefault();
            closeAllNotifications();
            
            if(checkName && checkSalary && checkDescriptionShort && checkScope && checkRequirements && checkBenefits) {
                sendData('addOffer');
            } else {
                var msg_name, msg_salary, msg_description, msg_scope, msg_requirements, msg_benefits;
                if(!checkName) msg_name = '\n   - Chýba názov ponuky'; else msg_pass = '';
                if(!checkSalary) msg_salary = '\n   - Zlý tvar platu (len čísla, bez medzier)'; else msg_pass = '';
                if(!checkDescriptionShort) msg_description = '\n   - Chýba krátky popis'; else msg_pass = '';
                if(!checkScope) msg_scope = '\n   - Chýba popis'; else msg_email = '';
                if(!checkRequirements) msg_requirements = '\n   - Chýbajú požiadavky'; else msg_ico = '';
                if(!checkBenefits) msg_benefits = '\n   - Chýbajú benefity'; else msg_tel = '';

                showNotification('Pridanie ponuky nebolo úspešné', 'Je nutné upraviť polia:'+msg_name+msg_salary+msg_description+msg_scope+msg_requirements+msg_benefits);
            }
        });
    }

    function sendData(formType) {
        var formElement = document.querySelector("form");
        var request = new XMLHttpRequest();

        switch(formType){
            case 'addOffer':
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var result = JSON.parse(this.responseText);
                        switch(result['code']) {
                            case 200:
                                redirect('/', 'Vytvorenie ponuky bolo úspešné');
                                break;
                            case 400:
                                showNotification('Chyba', 'Vytvorenie ponuky zlyhalo!');
                                break;
                        }
                    }
                };
        
                request.open("POST", "/pridat-ponuku");
                request.send(new FormData(formElement));
                break;
        }
    }

    var btn_search = document.getElementById("search");
    var dropdown = document.getElementsByTagName("dropdown");
    for(var i = 0; i < dropdown.length; i++) {
        dropdownSelected[i] = {'element':dropdown[i], 'original':dropdown[i].children[0].innerHTML, 'active':null};
        for(var d_i = 0, d_list = dropdown[i].children[1].children; d_i < d_list.length; d_i++) {
            if(d_i > 0) {
                d_list[d_i].onclick = function (){
                    var itemName = this.innerHTML;
                    r_itemName = itemName.replace(new RegExp(/[ľ]/g),"l");
                    r_itemName = itemName.replace(new RegExp(/[š]/g),"s");
                    r_itemName = itemName.replace(new RegExp(/[č]/g),"c");
                    r_itemName = itemName.replace(new RegExp(/[ť]/g),"t");
                    r_itemName = itemName.replace(new RegExp(/[ž]/g),"z");
                    r_itemName = itemName.replace(new RegExp(/[ý]/g),"y");
                    r_itemName = itemName.replace(new RegExp(/[á]/g),"a");
                    r_itemName = itemName.replace(new RegExp(/[í]/g),"i");
                    r_itemName = itemName.replace(new RegExp(/[é]/g),"e");
                    r_itemName = itemName.replace(new RegExp(/[ä]/g),"a");
                    r_itemName = itemName.replace(new RegExp(/[ú]/g),"u");
                    r_itemName = itemName.replace(new RegExp(/[ň]/g),"n");
                    r_itemName = itemName.replace(new RegExp(/[ô]/g),"o");
                    r_itemName = itemName.replace(" ","-");

                    for(var i = 0, len = dropdown.length; i < len; i++) {
                        if(this.parentElement.parentElement === dropdownSelected[i]['element']) {
                            dropdownSelected[i]['active'] = r_itemName.toLowerCase();
                            this.parentElement.previousElementSibling.innerHTML = itemName;
                            i = len;
                        }
                    }
                }
            } else {
                d_list[d_i].onclick = function (){
                    for(var i = 0, len = dropdown.length; i < len; i++) {
                        if(this.parentElement.parentElement === dropdownSelected[i]['element']) {
                            dropdownSelected[i]['active'] = null;
                            this.parentElement.previousElementSibling.innerHTML = dropdownSelected[i]['original'];
                            i = len;
                        }
                    }
                }
            }
        }
        dropdown[i].onclick = function() {
            if(activeElement !== undefined) {
                if(activeElement === this) {
                    activeElement.children[1].className += " hidden";
                    activeElement = undefined;
                } else {
                    activeElement.children[1].className += " hidden";
                    activeElement = undefined;
                    activeElement = this.children[1].parentElement;
                    this.children[1].classList.remove("hidden");
                }
            } else {
                activeElement = this.children[1].parentElement;
                this.children[1].classList.remove("hidden");
            }
        };
    }

    btn_search.addEventListener("click", function(e){
        var inputField = this.parentElement.children[2].value;

        e.preventDefault();

        if(dropdownSelected[0]['active'] === null && dropdownSelected[0]['active'] === null && inputField == "") {
            window.location.href = "/ponuky-prace";
        }
        else {
            var getParams = "?";
            if(dropdownSelected[0]['active'] !== null)
                getParams += "c="+dropdownSelected[0]['active']+"&";
            if(dropdownSelected[1]['active'] !== null)
                getParams += "l="+dropdownSelected[1]['active']+"&";
            if(this.parentElement.children[2].value != "")
                getParams += "s="+inputField+"&";
            
            window.location.href = "/ponuky-prace/hladat"+getParams.substr(0, getParams.length-1);
        }
    });

    check_fields();
    createSwitch();
};

window.onclick = function(event) {
    if(activeElement !== undefined) {
        if(event.target != activeElement.children[0]) {
            activeElement.children[1].className += " hidden";
            activeElement = undefined;
        }
    }
}

function check_fields() {
    var collection_input = document.getElementsByTagName('input');
    var collection_textarea = document.getElementsByTagName('textarea');
    
    var field_offer_name;
    var field_offer_salary;

    var area_offer_description_short;
    var area_offer_scope;
    var area_offer_requirements;
    var area_offer_benefits;

    if(collection_input.length > 0){
        for(var i = 0; i < collection_input.length; i++) {
            if(collection_input[i].name.toLowerCase() == "offer_name") {
                field_offer_name = collection_input[i];
            } else if(collection_input[i].name.toLowerCase() == "offer_salary") {
                field_offer_salary = collection_input[i];
            }
        }

        if(field_offer_name !== undefined){
            field_offer_name.addEventListener("change", function(){
                if(field_offer_name.value == "") {
                    field_offer_name.style.border = '2px solid red';
                    checkName = false;
                } else {
                    field_offer_name.style.border = '';
                    checkName = true;
                }
            });
        }

        if(field_offer_salary !== undefined){
            field_offer_salary.addEventListener("change", function(){
                if(field_offer_salary.value != ""){
                    var reg = /^[0-9]+$/;
                    if(reg.test(field_offer_salary.value)){
                        field_offer_salary.style.border = '';
                        checkSalary = true;
                    } else {
                        field_offer_salary.style.border = '2px solid red';
                        checkSalary = false;
                    }
                } else {
                    field_offer_salary.style.border = '';
                    checkSalary = true;
                }
            });
        }
    }

    if(collection_textarea.length > 0){
        for(var i = 0; i < collection_textarea.length; i++) {
            if(collection_textarea[i].name.toLowerCase() == 'offer_description_short') {
                area_offer_description_short = collection_textarea[i];
            } else if(collection_textarea[i].name.toLowerCase() == "offer_scope") {
                area_offer_scope = collection_textarea[i];
            } else if(collection_textarea[i].name.toLowerCase() == "offer_requirements") {
                area_offer_requirements = collection_textarea[i];
            } else if(collection_textarea[i].name.toLowerCase() == "offer_benefits") {
                area_offer_benefits = collection_textarea[i];
            }
        }

        area_offer_description_short.addEventListener("change", function(){
            if(area_offer_description_short.value == "") {
                area_offer_description_short.style.border = '2px solid red';
                checkDescriptionShort = false;
            } else {
                area_offer_description_short.style.border = '';
                checkDescriptionShort = true;
            }
        });

        area_offer_scope.addEventListener("change", function(){
            if(area_offer_scope.value == "") {
                area_offer_scope.style.border = '2px solid red';
                checkScope = false;
            } else {
                area_offer_scope.style.border = '';
                checkScope = true;
            }
        });

        area_offer_requirements.addEventListener("change", function(){
            if(area_offer_requirements.value == "") {
                area_offer_requirements.style.border = '2px solid red';
                checkRequirements = false;
            } else {
                area_offer_requirements.style.border = '';
                checkRequirements = true;
            }
        });

        area_offer_benefits.addEventListener("change", function(){
            if(area_offer_benefits.value == "") {
                area_offer_description_short.style.border = '2px solid red';
                checkBenefits = false;
            } else {
                area_offer_benefits.style.border = '';
                checkBenefits = true;
            }
        });
    }
}

function switchChange(val){
    if(switchData[val] === undefined) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                switchData[val] = JSON.parse(xhttp.responseText);
                addItems(val);
            }
        };
        xhttp.open("GET", "/ponuky-prace/get/"+((val-1)*10), true);
        xhttp.send();
    } else {
        addItems(val);
    }

    function addItems(id) {
        var fields = document.getElementById("offers").children[1];
        var elem_a = document.createElement("a");
        var elem_logo = document.createElement("logo");
        var elem_img = document.createElement("img");
        var elem_informations = document.createElement("informations");
        var elem_h3 = document.createElement("h3");
        var elem_p_l = document.createElement("p");
        elem_p_l.className = "location";
        var elem_p_s = document.createElement("p");
        elem_p_s.className = "salary";
        var elem_p_t = document.createElement("p");
        elem_p_t.className = "time";
        var elem_date = document.createElement("date");

        fields.innerHTML = "";

        for(i = 0; i < switchData[id].length; i++){
            node_h3 = document.createTextNode(switchData[id][i]['name']);
            node_p_l = document.createTextNode(switchData[id][i]['location']);
            node_p_s = document.createTextNode(switchData[id][i]['salary']);
            node_p_t = document.createTextNode(switchData[id][i]['time']);
            node_date = document.createTextNode(switchData[id][i]['created_on']);

            clone_a = elem_a.cloneNode(true);
            clone_logo = elem_logo.cloneNode(true);
            clone_img = elem_img.cloneNode(true);
            clone_info = elem_informations.cloneNode(true);
            clone_h3 = elem_h3.cloneNode(true);
            clone_h3.appendChild(node_h3);
            clone_p_l = elem_p_l.cloneNode(true);
            clone_p_l.appendChild(node_p_l);
            clone_p_s = elem_p_s.cloneNode(true);
            clone_p_s.appendChild(node_p_s);
            clone_p_t = elem_p_t.cloneNode(true);
            clone_p_t.appendChild(node_p_t);
            clone_date = elem_date.cloneNode(true);
            clone_date.appendChild(node_date);

            clone_a.href = "/ponuka-prace/"+switchData[id][i]['id'];
            clone_img.src = switchData[id][i]['logo'];
            clone_logo.appendChild(clone_img);
            clone_a.appendChild(clone_logo);
            clone_info.appendChild(clone_h3);
            clone_info.appendChild(clone_p_l);
            clone_info.appendChild(clone_p_s);
            clone_info.appendChild(clone_p_t);
            clone_a.appendChild(clone_info);
            clone_a.appendChild(clone_date);
            fields.appendChild(clone_a);
        }
    }
}