var dropdownSelected = {};
var activeElement;

window.onload = function() {
    var btn_search = document.getElementById("search");
    var dropdown = document.getElementsByTagName("dropdown");
    for(var i = 0; i < dropdown.length; i++) {
        dropdownSelected[i] = {'element':dropdown[i], 'original':dropdown[i].children[0].innerHTML, 'active':null};
        for(var d_i = 0, d_list = dropdown[i].children[1].children; d_i < d_list.length; d_i++) {
            if(d_i > 0) {
                d_list[d_i].onclick = function (){
                    var itemName = this.innerHTML;
                    r_itemName = itemName.replace(new RegExp(/[ľ]/g),"l");
                    r_itemName = itemName.replace(new RegExp(/[š]/g),"s");
                    r_itemName = itemName.replace(new RegExp(/[č]/g),"c");
                    r_itemName = itemName.replace(new RegExp(/[ť]/g),"t");
                    r_itemName = itemName.replace(new RegExp(/[ž]/g),"z");
                    r_itemName = itemName.replace(new RegExp(/[ý]/g),"y");
                    r_itemName = itemName.replace(new RegExp(/[á]/g),"a");
                    r_itemName = itemName.replace(new RegExp(/[í]/g),"i");
                    r_itemName = itemName.replace(new RegExp(/[é]/g),"e");
                    r_itemName = itemName.replace(new RegExp(/[ä]/g),"a");
                    r_itemName = itemName.replace(new RegExp(/[ú]/g),"u");
                    r_itemName = itemName.replace(new RegExp(/[ň]/g),"n");
                    r_itemName = itemName.replace(new RegExp(/[ô]/g),"o");
                    r_itemName = itemName.replace(" ","-");

                    for(var i = 0, len = dropdown.length; i < len; i++) {
                        if(this.parentElement.parentElement === dropdownSelected[i]['element']) {
                            dropdownSelected[i]['active'] = r_itemName.toLowerCase();
                            this.parentElement.previousElementSibling.innerHTML = itemName;
                            i = len;
                        }
                    }
                }
            } else {
                d_list[d_i].onclick = function (){
                    for(var i = 0, len = dropdown.length; i < len; i++) {
                        if(this.parentElement.parentElement === dropdownSelected[i]['element']) {
                            dropdownSelected[i]['active'] = null;
                            this.parentElement.previousElementSibling.innerHTML = dropdownSelected[i]['original'];
                            i = len;
                        }
                    }
                }
            }
        }
        dropdown[i].onclick = function() {
            if(activeElement !== undefined) {
                if(activeElement === this) {
                    activeElement.children[1].className += " hidden";
                    activeElement = undefined;
                } else {
                    activeElement.children[1].className += " hidden";
                    activeElement = undefined;
                    activeElement = this.children[1].parentElement;
                    this.children[1].classList.remove("hidden");
                }
            } else {
                activeElement = this.children[1].parentElement;
                this.children[1].classList.remove("hidden");
            }
        };
    }

    btn_search.addEventListener("click", function(e){
        var inputField = this.parentElement.children[2].value;

        e.preventDefault();

        if(dropdownSelected[0]['active'] === null && dropdownSelected[0]['active'] === null && inputField == "") {
            window.location.href = "/ponuky-prace";
        }
        else {
            var getParams = "?";
            if(dropdownSelected[0]['active'] !== null)
                getParams += "c="+dropdownSelected[0]['active']+"&";
            if(dropdownSelected[1]['active'] !== null)
                getParams += "l="+dropdownSelected[1]['active']+"&";
            if(this.parentElement.children[2].value != "")
                getParams += "s="+inputField+"&";
            
            window.location.href = "/ponuky-prace/hladat"+getParams.substr(0, getParams.length-1);
        }
    });
}

window.onclick = function(event) {
    if(activeElement !== undefined) {
        if(event.target != activeElement.children[0]) {
            activeElement.children[1].className += " hidden";
            activeElement = undefined;
        }
    }
}