document.createElement("container");
document.createElement("part-left");
document.createElement("part-right");
document.createElement("fields");
document.createElement("background");
document.createElement("suhlas");
document.createElement("search");
document.createElement("dropdown");
document.createElement("logo");
document.createElement("informations");
document.createElement("date");
document.createElement("switch");
document.createElement("buttons");

var switchActive = 1;
var switchLength = 0;

function createSwitch() {
    var switchs = document.getElementsByTagName('switch');

    if(switchs.length > 0 ){
        var elem = document.createElement("p");
        elem.style.fontFamily = "FontAwesome";
        elem.style.fontSize = "15px";

        var left = elem.cloneNode(true);
        var nodeLeft = document.createTextNode("");
        left.appendChild(nodeLeft);

        var right = elem.cloneNode(true);
        var nodeRight = document.createTextNode("");
        right.appendChild(nodeRight);

        for(var i = 0; i < switchs.length; i++) {
            var cloneLeft = left.cloneNode(true);
            var cloneRight = right.cloneNode(true);

            for(var c_i = 0; c_i < switchs[i].children.length; c_i++){
                switchs[i].children[c_i].addEventListener("click", function(){
                    if(this !== this.parentElement.children[switchActive]){
                        this.parentElement.children[switchActive].classList.remove("active");
                        switchActive = this.innerHTML;
                        switchChange(switchActive);
                        this.className = "active";
                    }
                });
            }
            

            cloneLeft.addEventListener("click", function(){
                if(switchLength == 0)
                    switchItems = document.getElementsByTagName("switch")[0].children;
                    switchLength = switchItems.length - 2;

                if(switchActive == 1){
                    switchItems[switchActive].classList.remove("active"); 
                    switchActive = switchLength;
                    switchChange(switchActive);
                    switchItems[switchActive].className = "active";
                } else {
                    switchItems[switchActive].classList.remove("active"); 
                    switchActive = switchActive - 1;
                    switchChange(switchActive);
                    switchItems[switchActive].className = "active";
                }
            });

            cloneRight.addEventListener("click", function(){
                if(switchLength == 0)
                switchItems = document.getElementsByTagName("switch")[0].children;
                switchLength = switchItems.length - 2;

                if(switchActive == switchLength){
                    switchItems[switchActive].classList.remove("active"); 
                    switchActive = 1;
                    switchChange(switchActive);
                    switchItems[switchActive].className = "active";
                } else {
                    switchItems[switchActive].classList.remove("active"); 
                    switchActive = switchActive + 1;
                    switchChange(switchActive);
                    switchItems[switchActive].className = "active";
                }
            });

            switchs[i].insertBefore(cloneLeft, switchs[i].childNodes[0]);
            switchs[i].children[1].className = "active";
            switchs[i].appendChild(cloneRight);
        }
    }
}

function redirect(destination, message, timer) {
    timer = timer || 0;

    if(timer > 0){
        timer = timer * 1000;

        showNotification('Presmerovanie', message, timer);
        setTimeout(function(){
            post(destination, {header: 'Presmerovanie', message: message});
        }, timer);
    } else {
        post(destination, {header: 'Presmerovanie', message: message});
    }
}

function showNotification(header, details, timer) {
    timer = timer || 0;

    var elem_notification = document.createElement("notification");
    elem_notification.style.opacity = 1;

    var elem_close = document.createElement("close");
    var node_close = document.createTextNode("");
    elem_close.appendChild(node_close);
    elem_close.addEventListener("click", function(){
        clearTimeout(closeTimeout);
        var notification = this.parentElement;
        var fade = setInterval(function(){
            if(notification.style.opacity > 0) {
                notification.style.opacity -= 0.1;
            } else {
                clearInterval(fade);
                notification.parentNode.removeChild(notification);
            }
        }, 50);
    });

    var elem_h1 = document.createElement("h1");
    var node_header = document.createTextNode(header);
    elem_h1.appendChild(node_header);

    var elem_p = document.createElement("p");
    var node_details = document.createTextNode(details);
    elem_p.appendChild(node_details);

    document.body.appendChild(elem_notification);
    elem_notification.appendChild(elem_close);
    elem_notification.appendChild(elem_h1);
    elem_notification.appendChild(elem_p);

    if(timer > 0){
        timer = timer * 1000;
        var closeTimeout = setTimeout(function(){
            var fade = setInterval(function(){
                if(elem_notification.style.opacity > 0) {
                    elem_notification.style.opacity -= 0.1;
                } else {
                    clearInterval(fade);
                    elem_notification.parentNode.removeChild(elem_notification);
                }
            }, 50)}, timer);
    }
}

function closeAllNotifications(){
    collection_notification = document.getElementsByTagName('notification');
    for(var i = 0; i < collection_notification.length; i++){
        collection_notification[i].parentNode.removeChild(collection_notification[i]);
    }
}

// https://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit
// example: post('/contact/', {name: 'Johnny Bravo'});
function post(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
